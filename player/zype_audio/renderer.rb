# frozen_string_literal: true

class Player
  class ZypeAudio
    class Renderer
      include Player::Manifest

      private

      # override
      def player_logo; end

      # override
      def player_age_gate; end

      # override
      def player_sharing; end

      # override
      def player_ad; end

      # rubocop:disable Metrics/MethodLength
      def player
        @_player ||= {
          playlist: [{
            sources: audio_files,
            title: @data_source.video.title,
            mediaid: @data_source.video.id.to_s
          }],
          plugins: {},
          androidhls: true,
          autostart: @options[:autoplay] ? true : false,
          flashplayer: content_url('/jwplayer/6.11/jwplayer.flash.swf'),
          height: '30px',
          html5player: content_url('/jwplayer/6.11/jwplayer.html5.js'),
          primary: APP_CONFIG[:player_default_mode],
          skin: APP_CONFIG[:player_default_skin],
          width: '100%'
        }
      end
      # rubocop:enable Metrics/MethodLength

      # uniq
      def audio_outputs
        @data_source.outputs.in(preset_id: @data_source.site.audio_preset_ids)
      end

      # uniq
      def audio_files
        audio_outputs.collect { |o| { file: o.download_url, label: o.bitrate } }
      end
    end
  end
end
