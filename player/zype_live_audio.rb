# frozen_string_literal: true

require 'aes_crypt'

class Player
  class ZypeLiveAudio < Player
    field :on_air_required, type: :boolean, default: true
    field :audio_required, type: :boolean, default: true
  end
end
