# frozen_string_literal: true

class Player
  module Manifest
    def initialize(data_source, options = {})
      @data_source = data_source
      @options = options
      @referer_https = detect_referer_https
    end

    # build out base player with media information,
    # core settings including width, height, aspect ratio
    # auto start, skin
    def render
      player_logo
      player_age_gate
      player_ga
      player_sharing
      player_ad

      player.to_json
    end

    private

    # if player logo is present merge in the plugin
    def player_logo
      player.merge!(logo_plugin) if @data_source.site.player_logo.present?
    end

    # if age gate is required merge in the plugin
    def player_age_gate
      return unless @data_source.video.age_gate_required?

      player[:plugins].merge!(age_gate_plugin)
      # disable autostart when age gate enabled
      player[:autostart] = false
    end

    # if google analytics is required merge in the plugin
    def player_ga
      player.merge!(ga_plugin) if @data_source.site.ga_enabled?
    end

    def player_sharing
      player.merge!(sharing: {}) if @data_source.site.player_sharing_enabled?
    end

    def player_ad
      ad_tag = @options[:ad_tag]
      return unless ad_tag

      ad_tag.web_render(player, @data_source, @options)
    end

    def referer_https?
      @referer_https == true
    end

    def detect_referer_https
      return APP_CONFIG[:player_https] if @options[:iframe]

      uri = URI.parse(@options[:referer])
      uri.scheme == 'https'
    rescue StandardError
      Rails.logger.warn("Could not determine scheme from referer: #{@options[:referer]}, defaulting to #{APP_CONFIG[:player_https]}")
      APP_CONFIG[:player_https]
    end

    def manifest_params
      { video_id: @data_source.video.id, expires_at: manifest_expiration }
    end

    def manifest_token
      ManifestToken.new(manifest_params).encrypt(@data_source.video.signing_key)
    end

    def manifest_expiration
      @data_source.video.site.player_expiration.seconds.since.to_i
    end

    def manifest_url
      options = {
        host: APP_CONFIG[:manifest_host],
        port: (APP_CONFIG[:manifest_https] ? APP_CONFIG[:https_port] : APP_CONFIG[:http_port]),
        path: "/manifest/#{@data_source.video.id}.m3u8",
        query: Rack::Utils.build_query(player_key: @data_source.video.player_key, token: manifest_token)
      }

      (APP_CONFIG[:manifest_https] ? URI::HTTPS : URI::HTTP).build(options).to_s
    end

    def content_url(path)
      options = {
        host: APP_CONFIG[:content_host],
        port: (referer_https? ? APP_CONFIG[:https_port] : APP_CONFIG[:http_port]),
        path: path
      }

      (referer_https? ? URI::HTTPS : URI::HTTP).build(options).to_s
    end

    def ga_plugin
      {
        ga: {
          idstring: 'title',
          trackingobject: @data_source.video.site.ga_object,
          label: 'title'
        }
      }
    end
  end
end
