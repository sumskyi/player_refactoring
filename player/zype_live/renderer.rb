# frozen_string_literal: true

class Player
  class ZypeLive
    class Renderer < Player::Zype::Renderer
      include Player::Manifest

      private

      # override
      def manifest_expiration
        @data_source.expire_at.to_i
      end
    end
  end
end
