# frozen_string_literal: true

require 'aes_crypt'

class Player
  class Zype < Player
    field :ad_tag_required, type: :boolean, default: true
  end
end
