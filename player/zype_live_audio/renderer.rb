# frozen_string_literal: true

class Player
  class ZypeLiveAudio
    class Renderer
      include Player::Manifest

      private

      # override
      def player_logo; end

      # override
      def player_age_gate; end

      # override
      def player_sharing; end

      # override
      def player_ad; end

      # override
      def manifest_expiration
        @data_source.expire_at.to_i
      end

      # rubocop:disable Metrics/MethodLength
      def player
        @player ||= {
          playlist: [{
            sources: [{ file: manifest_url }],
            title: @data_source.video.title,
            mediaid: @data_source.video.id.to_s
          }],
          plugins: {},
          androidhls: true,
          autostart: @options[:autoplay] ? true : false,
          flashplayer: content_url('/jwplayer/6.11/jwplayer.flash.swf'),
          height: 30,
          html5player: content_url('/jwplayer/6.11/jwplayer.html5.js'),
          primary: APP_CONFIG[:player_default_mode],
          skin: APP_CONFIG[:player_default_skin],
          width: '100%'
        }
      end
      # rubocop:enable Metrics/MethodLength

      # override
      def manifest_params
        super.merge(audio: true)
      end
    end
  end
end
