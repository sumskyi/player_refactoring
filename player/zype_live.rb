# frozen_string_literal: true

class Player
  class ZypeLive < Player
    field :on_air_required, type: :boolean, default: true
  end
end
