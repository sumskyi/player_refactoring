# frozen_string_literal: true

class Player
  class Zype
    class Renderer
      include Player::Manifest

      private

      # rubocop:disable Metrics/MethodLength
      # rubocop:disable Metrics/AbcSize
      def player
        @_player ||= {
          playlist: [{
            sources: [{ file: manifest_url }],
            image: default_thumbnail_url,
            title: @data_source.video.title,
            mediaid: @data_source.video.id.to_s,
            tracks: subtitles

          }],
          plugins: {},
          androidhls: true,
          aspectratio: '16:9',
          autostart: @options[:autoplay] ? true : false,
          flashplayer: content_url('/jwplayer/6.11/jwplayer.flash.swf'),
          html5player: content_url('/jwplayer/6.11/jwplayer.html5.js'),
          primary: APP_CONFIG[:player_default_mode],
          skin: APP_CONFIG[:player_default_skin],
          width: '100%',
          abouttext: @data_source.site.title,
          aboutlink: @data_source.site.player_logo_link
        }
      end
      # rubocop:enable Metrics/MethodLength
      # rubocop:enable Metrics/AbcSize

      # uniq
      def logo_plugin
        {
          logo: {
            file: @data_source.site.player_logo.url(:thumb),
            link: @data_source.site.player_logo_link,
            margin: @data_source.site.player_logo_margin,
            position: @data_source.site.player_logo_position,
            hide: @data_source.site.player_logo_hide
          }
        }
      end

      # uniq
      def subtitles
        @data_source.video.subtitles.active.order(language: :asc).collect do |s|
          { file: s.file.url,
            label: s.language_name,
            kind: 'captions' }
        end
      end

      # uniq
      def age_gate_plugin
        {
          content_url('/jwplayer/agegate.js') => {
            cookielife: 60,
            minage: @data_source.video.site.age_gate_min_age
          }
        }
      end

      # uniq
      def default_thumbnail_url
        t = @data_source.thumbnails.max_by(&:height)
        return unless t

        t.url
      end
    end
  end
end
