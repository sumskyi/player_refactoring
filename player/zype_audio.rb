# frozen_string_literal: true

require 'aes_crypt'

class Player
  class ZypeAudio < Player
    field :audio_required, type: :boolean, default: true
  end
end
